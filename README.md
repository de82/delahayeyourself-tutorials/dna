# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### 🧬 DNA

> Le but de ce projet est de transformer un fichier csv d'entrée en un fichier csv de sortie dont le format (délimiteur, ordre des colonnes, etc.) est différent de celui d'entrée.


#### 🎯 Objectif

Notre script devra recevoir en paramètre le chemin vers le fichier csv et produire un fichier csv en sortie contenant les informations du fichier d'entrée au nouveau format, voici un exemple de l'appel de ce script:

```bash
$ transform.py /home/nedry/dna.csv
```

##### 🚧 Exemple d'entrée/sortie attendu

`CSV IN`

```csv
col1~col2
Dilophosaurus~tcgtgtcaaatagggcttcc
Velociraptor~tagtggaccaattactggac
Brachiosaurus~tcgctatccacccaacccgg
Parasaurolophus~atgcactatcactatcgttc
Triceratops~gtgatattatgccctcgcga
Tyrannosaurus Rex~cgctacggagaccggccttg
Gallimimus~catgccatctggagagtttc
```


`CSV OUT`

```csv
dna;name
TCGTGTCAAATAGGGCTTCC;Dilophosaurus
TAGTGGACCAATTACTGGAC;Velociraptor
TCGCTATCCACCCAACCCGG;Brachiosaurus
ATGCACTATCACTATCGTTC;Parasaurolophus
GTGATATTATGCCCTCGCGA;Triceratops
CGCTACGGAGACCGGCCTTG;Tyrannosaurus Rex
CATGCCATCTGGAGAGTTTC;Gallimimus
```

##### 📈 Diagramme de cas d'utilisation

![center](docs/usecases.png)

##### 📈 Diagramme d'activité

![center](docs/activity.png)

#### 🧵 Le projet

Pour lancer les tests unitaires (en se plaçant à la racine du projet):

```bash
$ python3 -m unittest
```

Pour créer la documentation du projet:

```bash
$ ./makedocs.sh
```

Pour lancer le projet:

```bash
python3 transform.py /home/nedry/dna.csv
```
